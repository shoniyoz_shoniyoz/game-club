export interface CategoryType {
  name: string;
  packet_name: [];
  packet_price: [];
  file: string;
  description: string;
  type: string;
  club_id: string;
}
