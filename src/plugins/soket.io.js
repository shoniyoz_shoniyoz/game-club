import SocketIO from "socket.io-client";
export const useSocketIO = () => {
  const socket = SocketIO("http://192.168.100.118:3000", {
    transports: ["websocket"],
  });
  return {
    socket,
  };
};
