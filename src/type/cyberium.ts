export interface Device {
  id: number;
  is_booking: boolean;
  pc_id: string;
  club_category_id: number;
  club_id: number;
  booking_time?: Date;
  d_id?: number;
  full_name?: string;
  status?: string;
  users?: string[];
  phone?: string;
}
interface Images {
  id: number;
  image: string;
}
interface Packets {
  id: number;
  name: number;
  price: number;
}

export interface CategoryType {
  id: number;
  name: string;
  type: string;
  packets: Packets[];
  images: Images[];
  description: string;
  club: {
    id: number;
    name: string;
    images: string[];
    description: string;
    phone: string;
  };
  device: Device[];
}

export interface HistoryType {
  id: number;
  device_id: number[];
  status: string;
  packet_id: number;
  type: string;
  price: number;
  full_name: string;
  phone: string;
  user: string[];
  booking_time: string;
  club_id: number;
  club_category_id: number;
}
