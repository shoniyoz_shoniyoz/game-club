import { h, reactive } from "vue";
import type { MenuOption } from "naive-ui";
import { RouterLink } from "vue-router";

export function useLayoutSidebar() {
  const menuOptions: MenuOption[] = reactive([
    {
      label: () =>
        h(
          RouterLink,
          {
            to: {
              name: "home",
            },
          },
          { default: () => "Главная" }
        ),
      key: "1",
    },
    {
      label: () =>
        h(
          RouterLink,
          {
            to: {
              name: "delivery",
            },
          },
          { default: () => "Доставка" }
        ),
      key: "2",
    },
    {
      label: () =>
        h(
          RouterLink,
          {
            to: {
              path: "/booking",
            },
          },
          { default: () => "Бронирование" }
        ),
      key: "3",
    },
    {
      label: () =>
        h(
          RouterLink,
          {
            to: {
              name: "restaurant",
            },
          },
          { default: () => "Ресторан" }
        ),
      key: "4",
    },
    {
      label: () =>
        h(
          RouterLink,
          {
            to: {
              name: "categoryDishes",
            },
          },
          { default: () => "Категория блюд" }
        ),
      key: "6",
    },
    {
      label: () =>
        h(
          RouterLink,
          {
            to: {
              name: "popularDishes",
            },
          },
          { default: () => "Добавит блюд" }
        ),
      key: "7",
    },
    {
      label: () =>
        h(
          RouterLink,
          {
            to: {
              name: "rooms",
            },
          },
          { default: () => "Номер" }
        ),
      key: "8",
    },
    {
      label: () =>
        h(
          RouterLink,
          {
            to: {
              name: "staff",
            },
          },
          { default: () => "Сотрудники" }
        ),
      key: "9",
    },
  ]);
  return menuOptions;
}
