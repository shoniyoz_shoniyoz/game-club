import { defineStore } from "pinia";
import $axios from "@/plugins/axios";
import { CategoryType, HistoryType } from "../type/cyberium";
import io from "socket.io-client";
const socket = io("http://192.168.100.118:3000", {
  withCredentials: true,
});
socket.emit("online", {
  club_admin_id: 1,
});

export const useCyberium = defineStore("cyberium", {
  state: () => ({
    history: [] as HistoryType[],
    categoryData: {} as CategoryType,
  }),
  actions: {
    async fetchCategories(params?: string) {
      return new Promise((resolve, reject) => {
        $axios
          .get("/api/admin/club/category")
          .then(({ data }) => {
            data.categories.forEach((value: CategoryType) => {
              if (value.name === params) {
                this.categoryData = value;
              }
            });

            resolve(data);
          })
          .catch((erorr) => {
            reject(erorr);
          });
      });
    },
    async fetchHistory() {
      return new Promise((resolve, reject) => {
        $axios
          .get("/api/admin/club/history")
          .then(({ data }) => {
            this.history = data.history;
            this.history.map((item: HistoryType) => ({
              ...item,
              device_id: item.device_id.map((id) => `${id}${item.type}`),
            }));
            socket.on("add_history", (newData) => {
              this.history.push(newData.history);
              this.fetchCategories().then(() => {
                this.history.forEach((historyItem: HistoryType) => {
                  historyItem.device_id.forEach((deviceId: number) => {
                    const device = this.categoryData.device.find(
                      (d) => d.id == deviceId
                    );
                    if (device) {
                      device.status = historyItem.status;
                      device.full_name = historyItem.full_name;
                      device.phone = historyItem.phone;
                      device.users = historyItem.user;
                      device.d_id = historyItem.id;
                      device.booking_time = new Date(historyItem.booking_time);
                    }
                  });
                });
              });
            });
            this.fetchCategories().then(() => {
              this.history.forEach((historyItem: HistoryType) => {
                historyItem.device_id.forEach((deviceId: number) => {
                  const device = this.categoryData.device.find(
                    (d) => d.id == deviceId
                  );
                  if (device) {
                    device.status = historyItem.status;
                    device.full_name = historyItem.full_name;
                    device.phone = historyItem.phone;
                    device.users = historyItem.user;
                    device.d_id = historyItem.id;
                    device.booking_time = new Date(historyItem.booking_time);
                  }
                });
              });
            });
            resolve(data);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  },
});
