import { defineStore } from "pinia";
import $axios from "../plugins/axios";
import { reactive } from "vue";

export const useStore = defineStore("main", () => {
  const data = reactive<{ [key: string]: any }>({});
  const fetchAction = async (
    url: string,
    keyState: string,
    params: { offset: number; limit: number }
  ) => {
    return new Promise((resolve, reject) => {
      $axios
        .get(url, {
          params,
        })
        .then((response) => {
          if (data[keyState] == undefined) {
            data[keyState] = response.data;
          } else {
            if (params?.offset !== 0) {
              data[keyState].results.push(...response.data.results);
            } else {
              data[keyState] = response.data;
            }
          }
          resolve(response.data);
        })
        .catch((err) => reject(err));
    });
  };

  const fetchActionWithoutParams = async (url: string, keyState: string) => {
    return new Promise((resolve, reject) => {
      $axios
        .get(url)
        .then((res) => {
          data[keyState] = res.data;
          resolve(res.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  const postAction = async (url: string, form: any) => {
    return new Promise((resolve, reject) => {
      $axios
        .post(url, {
          ...form,
        })
        .then((res) => {
          resolve(res.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };
  const editAction = async (url: string, form: any) => {
    return new Promise((resolve, reject) => {
      $axios
        .put(url, {
          ...form,
        })
        .then((res) => {
          resolve(res.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };
  const deleteAction = async (url: string) => {
    return new Promise((resolve, reject) => {
      $axios
        .delete(url)
        .then((res) => {
          resolve(res.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

  return {
    data,
    fetchAction,
    fetchActionWithoutParams,
    postAction,
    deleteAction,
    editAction,
  };
});
