import { defineStore } from "pinia";
import axios, { AxiosError, AxiosResponse } from "axios";

const $axios = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_IMAGE,
});

export const useImagesStore = defineStore("image", {
  state: () => ({
    images: [],
    image: null,
  }),
  actions: {
    async postImages(form: any) {
      return new Promise((resolve, reject) => {
        $axios
          .post("/files", form)
          .then((response: AxiosResponse) => {
            resolve(response);
          })
          .catch((error: AxiosError) => {
            console.log(error, "err");
            reject(error);
          });
      });
    },
    async postImage(form: any) {
      return new Promise((resolve, reject) => {
        $axios
          .post("/file", form)
          .then((response: AxiosResponse) => {
            resolve(response);
          })
          .catch((error: AxiosError) => {
            console.log(error, "err");
            reject(error);
          });
      });
    },
  },
});
