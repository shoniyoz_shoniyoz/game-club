import { createRouter, createWebHistory } from "vue-router";

function authGuard(to: any, from: any, next: any) {
  if (localStorage.getItem("token")) {
    next();
  } else {
    next("/login");
  }
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/login",
      name: "login",
      meta: { layout: "default" },
      component: () => import("../pages/login/PLIndex.vue"),
    },
    {
      path: "/",
      name: "home",
      meta: { layout: "main" },
      component: () => import("../pages/home/PHIndex.vue"),
    },
    {
      path: "/analytics",
      name: "analytics",
      meta: { layout: "main" },
      component: () => import("../pages/analytics/PAIndex.vue"),
    },
    {
      path: "/category",
      name: "category",
      meta: { layout: "main" },
      component: () => import("../pages/category/PCIndex.vue"),
    },

    {
      path: "/comments",
      name: "commnets",
      meta: { layout: "main" },
      component: () => import("../pages/comments/PCIndex.vue"),
    },
    {
      path: "/price",
      name: "price",
      meta: { layout: "main" },
      component: () => import("../pages/price/PPIndex.vue"),
    },
    {
      path: "/confirms",
      name: "confirms",
      meta: { layout: "main" },
      component: () => import("../pages/confrims/PCIndex.vue"),
    },
    {
      path: "/discounts",
      name: "discounts",
      meta: { layout: "main" },
      component: () => import("../pages/discounts/PDIndex.vue"),
    },
  ],
});

export default router;
